#include <iostream>
#include "agent.h"
using namespace std;

// Default constructor
Agent::Agent()
{
	name = " ";
	points = 100;
	num_turns = 0;
}

// Second constructor
Agent::Agent(string newName, unsigned long long int newPoint, int newNum_turns)
{
	name = newName;
	points = newPoint;
	num_turns = newNum_turns;
}

void Agent::print(vector<Agent*> &my_agents, int num_turns, int whose_turn)
{
	cout << "Turn: " << num_turns << ", Playing agent: " << my_agents[whose_turn]->get_name() << " (" << my_agents[whose_turn]->get_points() << ")" << endl
		<< "\t" << my_agents[(whose_turn + 1) % 3]->get_name() << " (" << my_agents[(whose_turn + 1) % 3]->get_points() << ") [" << (whose_turn + 1) % 3 << "]" << endl
		<< "\t" << my_agents[(whose_turn + 2) % 3]->get_name() << " (" << my_agents[(whose_turn + 2) % 3]->get_points() << ") [" << (whose_turn + 2) % 3 << "]\n";
}

// Input operator overloading
istream &operator >> (istream &in, Agent &my_Agent)
{
	in >> my_Agent.name;
	in >> my_Agent.points;
	in >> my_Agent.num_turns;
	return in;
}

// Output operator overloading
ostream &operator << (ostream &out, Agent my_Agent)
{
	out << my_Agent.name << endl << my_Agent.points << endl << my_Agent.num_turns << endl;
	return out;
}