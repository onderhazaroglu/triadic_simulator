#ifndef AGENT_H
#define AGENT_H

#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct act_pair
{
	act_pair() : p(0), action(0) { }
	act_pair(int a, int b) : p(a), action(b) 
	{
		if (p < 0) p += 3;
	}
	int p;
	int action;
};

class Agent
{
protected:
	string name;				// The agent's name
	unsigned long long int points;					// The agent's points
	int num_turns;				// The number of turns
	static vector<int> actions;	// All actions in terms of fractions (0-100%)

public:
	// Default constructor
	Agent();

	// Second constructor
	Agent(string newName, unsigned long long int newPoint, int newNum_turns);

	// Destructor
	~Agent() {};

	virtual act_pair play(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose) { return act_pair(); }

	static void print(vector<Agent*> &my_agents, int num_turns, int whose_turn);
	
	// Getter functions
	string get_name() { return name; }
	unsigned long long int get_points() { return points; }
	int get_num_turns() { return num_turns; }
	static vector<int> get_actions() { return actions; }

	// Setter functions
	void set_name(string newName) { name = newName; }
	void set_points(unsigned long long int newPoint) { points = newPoint; }
	void set_num_turns(int newNum_turns) { num_turns = newNum_turns; }

	void add_points(unsigned long long int newPoint) { points += newPoint; }
	void rem_points(unsigned long long int newPoint) { points -= newPoint; }

	// Friend functions for operator overloadings
	friend istream& operator >> (istream &in, Agent &my_Agent);
	friend ostream& operator << (ostream &out, Agent my_Agent);
};

#endif