#include <iostream>
#include <fstream>
#include <string>
#include "OptimalAgent.h"
using namespace std;

OptimalAgent::OptimalAgent() : Agent()
{ }

OptimalAgent::OptimalAgent(string newName, unsigned long long int newPoint, int newNum_turns) : Agent(newName, newPoint, newNum_turns)
{ }

act_pair OptimalAgent::play(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose)
{	
	cout << "Optimal is playing..." << endl;
	if (verbose) my_agents[whose_turn]->print(my_agents, num_turns, whose_turn);

	mxArray *fu, *fq = NULL;									// It creates two arrays (fu[0] is for fraction, fu[1] is for target id, and fq is for the state' q value.

	// char str3[] = "load(\'E:\\tce2_fittedqidemo.mat\')";		// It loads tce2_fittedqidemo.mat.
	// engEvalString(ep, str3);									// It runs the command in str3[].
	
	string state[] = { to_string(my_agents[whose_turn]->get_points()), to_string(my_agents[(whose_turn + 1) % 3]->get_points()), to_string(my_agents[(whose_turn + 2) % 3]->get_points()), to_string(num_turns) };
	string mystate = state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3];

	// cout << state[0] << state[1] << state[2] << state[3] << endl;

	cout << "check 1" << endl;

	map<string, pair<int,int>>::iterator it;
	it = utable.find(mystate);
	
	double *u = new double[2], *q;

	cout << "check 2" << endl;

	if ( it == utable.end())
	{
		// cout << "approx sorgu...\n";
		// cout << "new record: " << mystate << endl;
		// string mxcmd = "[u q]=approx.h(approx, [], [" + to_string(my_agents[whose_turn]->get_points()) + ";" + to_string(my_agents[(whose_turn + 1) % 3]->get_points()) + ";" + to_string(my_agents[(whose_turn + 2) % 3]->get_points()) + ";" + to_string(num_turns) + "])";
		// string mxcmd = "[u q]=approx.h(approx, [], [" + state[0] + ";" + state[1] + ";" + state[2] + ";" + state[3] + "])";
		string mxcmd = "[ u q ] = uq_query(" + state[0] + "," + state[1] + "," + state[2] + "," + state[3] + ",ST);";
		cout << mxcmd << endl;
		char *str4 = (char *)mxcmd.c_str();
		engEvalString(ep, str4);									// It runs the command in str4[].

		cout << "check 3" << endl;

		fu = engGetVariable(ep, "u");								// It gets the u value, and assigns it to fu.
		fq = engGetVariable(ep, "q");								// It gets the q value, and assigns it to fq.

		cout << "check 3.5" << endl;

		// int *u;
		u = (double *)mxGetData(fu);
		// cout << "pt: " << u[0] << endl;
		// cout << "tg: " << u[1] << endl;
		q = (double *)mxGetData(fq);

		cout << "check 4" << endl;

		// cout << "act: " << u[0] << " tg: " << u[1] << endl;

		// utable.insert(std::pair<string, int*>(state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3], u));
		// utable.insert({ state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3], u });
		utable.emplace(state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3], pair<int,int>(int(u[0]),int(u[1])) );

		cout << "u[0] = " << int(u[0]) << " , u[1] = " << int(u[1]) << endl;
		
		// cout << utable[mystate][0] << endl;
		// cout << utable[mystate][1] << endl;

		// utable.insert(state[0]+"-"+state[1]+"-"+state[2]+"-"+state[3]) = u;

		// cout << "bitti 1" << endl;
		mxDestroyArray(fu);
		mxDestroyArray(fq);
	}
	else
	{
		// cout << "found record: " << mystate << endl;
		// cout << it->second.first << endl;
		u[0] = it->second.first;
		u[1] = it->second.second;
		// u[1] = (it->second)[1];
		// cout << "hizla map den\n";
	}

	// cout << u[0] << " " << u[1] << endl;
	// cout << "bitti 2" << endl;
	vector<int> targets;	// It keeps eligible target ids except the player.

	// It fills up the targets vector.
	for (unsigned int i = 0; i < my_agents.size(); i++) if (i != whose_turn) targets.push_back(i);

	int targeti = int(u[1]);									// It assigns the id in the targets.
	int target_num = (whose_turn + targeti) % my_agents.size();	// The actual target id in the players
	if (verbose) cout << "target: " << target_num << "(" << my_agents[target_num]->get_name() <<"), ";

	// unsigned int action = unsigned int(u[0]);							// It takes the optimal action.
	unsigned int action = unsigned int(u[0]);										// It takes the optimal action.
	// if (verbose) cout << "action: " << action << Agent::actions[action] << "\n\n";
	if (verbose) cout << "action: " << action << "\n\n";


	Agent *me = my_agents[whose_turn];							// A pointer for the player
	Agent *target = my_agents[target_num];						// A pointer for the target

	unsigned long long int points = me->get_points() * action / 100;				// The actual points in action

	me->rem_points(points);										// The points are removed from the player.
	target->add_points(3 * points);								// 3 x points are added to the target.

	return act_pair((target_num - whose_turn) % 3, action);
}

act_pair OptimalAgent::oldplay(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose)
{
	if (verbose) my_agents[whose_turn]->print(my_agents, num_turns, whose_turn);

	mxArray *fu, *fq = NULL;									// It creates two arrays (fu[0] is for fraction, fu[1] is for target id, and fq is for the state' q value.

																// char str3[] = "load(\'E:\\tce2_fittedqidemo.mat\')";		// It loads tce2_fittedqidemo.mat.
																// engEvalString(ep, str3);									// It runs the command in str3[].

	string state[] = { to_string(my_agents[whose_turn]->get_points()), to_string(my_agents[(whose_turn + 1) % 3]->get_points()), to_string(my_agents[(whose_turn + 2) % 3]->get_points()), to_string(num_turns) };
	string mystate = state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3];

	// cout << state[0] << state[1] << state[2] << state[3] << endl;

	map<string, pair<int, int>>::iterator it;
	it = utable.find(mystate);

	double *u = new double[2], *q;

	if (it == utable.end())
	{
		// cout << "approx sorgu...\n";
		// cout << "new record: " << mystate << endl;
		// string mxcmd = "[u q]=approx.h(approx, [], [" + to_string(my_agents[whose_turn]->get_points()) + ";" + to_string(my_agents[(whose_turn + 1) % 3]->get_points()) + ";" + to_string(my_agents[(whose_turn + 2) % 3]->get_points()) + ";" + to_string(num_turns) + "])";
		string mxcmd = "[u q]=approx.h(approx, [], [" + state[0] + ";" + state[1] + ";" + state[2] + ";" + state[3] + "])";
		char *str4 = (char *)mxcmd.c_str();
		engEvalString(ep, str4);									// It runs the command in str4[].

		fu = engGetVariable(ep, "u");								// It gets the u value, and assigns it to fu.
		fq = engGetVariable(ep, "q");								// It gets the q value, and assigns it to fq.

																	// int *u;
		u = (double *)mxGetData(fu);
		// cout << "pt: " << u[0] << endl;
		// cout << "tg: " << u[1] << endl;
		q = (double *)mxGetData(fq);

		// cout << "act: " << u[0] << " tg: " << u[1] << endl;

		// utable.insert(std::pair<string, int*>(state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3], u));
		// utable.insert({ state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3], u });
		utable.emplace(state[0] + "-" + state[1] + "-" + state[2] + "-" + state[3], pair<int, int>(int(u[0]), int(u[1])));

		// cout << utable[mystate][0] << endl;
		// cout << utable[mystate][1] << endl;

		// utable.insert(state[0]+"-"+state[1]+"-"+state[2]+"-"+state[3]) = u;

		// cout << "bitti 1" << endl;
		mxDestroyArray(fu);
		mxDestroyArray(fq);
	}
	else
	{
		// cout << "found record: " << mystate << endl;
		// cout << it->second.first << endl;
		u[0] = it->second.first;
		u[1] = it->second.second;
		// u[1] = (it->second)[1];
		// cout << "hizla map den\n";
	}

	// cout << u[0] << " " << u[1] << endl;
	// cout << "bitti 2" << endl;
	vector<int> targets;	// It keeps eligible target ids except the player.

							// It fills up the targets vector.
	for (unsigned int i = 0; i < my_agents.size(); i++) if (i != whose_turn) targets.push_back(i);

	int targeti = int(u[1]);									// It assigns the id in the targets.
	int target_num = (whose_turn + targeti) % my_agents.size();	// The actual target id in the players
	if (verbose) cout << "target: " << target_num << "(" << my_agents[target_num]->get_name() << "), ";

	unsigned int action = unsigned int(u[0]);							// It takes the optimal action.
	if (verbose) cout << "action: " << action << Agent::actions[action] << "\n\n";

	Agent *me = my_agents[whose_turn];							// A pointer for the player
	Agent *target = my_agents[target_num];						// A pointer for the target

	unsigned long long int points = me->get_points() * action / 100;				// The actual points in action

	me->rem_points(points);										// The points are removed from the player.
	target->add_points(3 * points);								// 3 x points are added to the target.

	return act_pair((target_num - whose_turn) % 3, action);
}

bool OptimalAgent::loadmat()
{
	char str[] = "load(\'H:\\approx.mat\')";		// It loads tce2_fittedqidemo.mat.
	engEvalString(ep, str);									// It runs the command in str3[].

	if (engGetVariable(ep, "approx") == NULL)
	{
		cout << "Matlab data couldn't be loaded!\n\n";
		return false;
	}
	else {
		cout << "Matlab data is loaded.\n\n";
		return true;
	}
}

bool OptimalAgent::loadmap(string filename)
{
	string st;
	int u1, u2, count = 0;

	ifstream mapfile(filename);
	if (mapfile.fail())
	{
		cout << "Map file is not found." << endl;
		return false;
	}
	else
	{
		while (mapfile >> st >> u1 >> u2)
		{
			utable.emplace(st, pair<int, int>(u1, u2));
			count++;
		}
		mapfile.close();
		cout << "Map file (" << filename << ") is loaded (" << count << " records).\n" << endl;
		return true;
	}
}

bool OptimalAgent::savemap(string filename)
{
	int count = 0;
	ofstream mapfile(filename);
	if (mapfile.fail())
	{
		// cout << "Map file is not found." << endl;
		return false;
	}
	else
	{
		map<string, pair<int, int>>::iterator mp;
		for (mp = utable.begin(); mp != utable.end();mp++)
		{
			mapfile << mp->first << " " << mp->second.first << " " << mp->second.second << endl;
			count++;
		}
		mapfile.close();
		// cout << "Map file (" << filename << ") is saved (" << count << " records).\n" << endl;
		return true;
	}
}

bool OptimalAgent::loadmats2()
{
	bool mats_loaded = true;
	char loadcmd[] = "ST{1}=load('H:\\5k_approx\\approx5k1.mat'); run(\'H:\\Gokce\\Documents\\Projects\\triadic_simulator_cpp\\data\\multiple_approx_demo.m\')";
	engEvalString(ep, loadcmd);									// It runs the command in str3[].

	// char str[] = "load(\'H:\\approx.mat\')";		// It loads tce2_fittedqidemo.mat.
	// engEvalString(ep, str);									// It runs the command in str3[].

	char chcmd[50];
	for (int i = 1; i < 11; i++)
	{
		sprintf_s(chcmd, "ST{%d}.approx.U", i);
		if (engGetVariable(ep, chcmd) == NULL)
		{
			cout << "missing approx file " + to_string(i) + "!" << endl;
			mats_loaded = false;
		}
	}

	// if (engGetVariable(ep, "approx") == NULL)
	if (!mats_loaded)
	{
		cout << "Matlab data files couldn't be loaded!\n\n";
		return false;
	}
	else {
		cout << "Matlab data is loaded.\n\n";
		return true;
	}
}



bool OptimalAgent::loadmats()
{
	// char pathstr[] = "addpath(genpath('H:\\Gokce\\Documents\\Projects\\triadic_simulator_cpp\\data\\'))";
	char pathstr[] = "cd ..;";
	engEvalString(ep, pathstr);

	char str[] = "load(\'H:\\5k_approx\\all_approx.mat\')";		// It loads tce2_fittedqidemo.mat.
	engEvalString(ep, str);									// It runs the command in str3[].

	if (engGetVariable(ep, "ST") == NULL)
	{
		cout << "Matlab data couldn't be loaded!\n\n";
		return false;
	}
	else {
		cout << "Matlab data is loaded.\n\n";
		return true;
	}
}