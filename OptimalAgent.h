#pragma once

#include <iostream>
#include <map>
#include <utility>
#include "agent.h"
#include "matrix.h"
#include "engine.h"

#define  BUFSIZE 256
using namespace std;

class OptimalAgent : public Agent
{
public:
	static Engine *ep;
	static map<string, pair<int,int>> utable;

	static bool loadmat();
	static bool loadmats();
	static bool loadmats2();
	static bool loadmap(string filename);
	static bool savemap(string filename);

	OptimalAgent();
	OptimalAgent(string newName, unsigned long long int newPoint, int newNum_turns);

	~OptimalAgent() { }

	virtual act_pair play(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose);
	virtual act_pair oldplay(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose);
};