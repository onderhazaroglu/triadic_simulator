#pragma once

#include <iostream>
#include "agent.h"
using namespace std;

class RandomAgent : public Agent
{
public:
	RandomAgent();
	RandomAgent(string newName, unsigned long long int newPoint, int newNum_turns);

	~RandomAgent() { }

	virtual act_pair play(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose);
};