#include <ctime>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include "agent.h"
#include "RandomAgent.h"
#include "OptimalAgent.h"
#include "RealAgent.h"
using namespace std;

vector<int> Agent::actions = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };	// The actions in terms of percentages
string get_date();
string get_timestamp();
// string get_timestamp2();
Engine *OptimalAgent::ep = engOpen(NULL);
map<string, pair<int,int>> OptimalAgent::utable = {};

//                          1			2			3			4			5		6			7				8
// command line parameters: a_type1,	a_type_2,	a_type3,	n_games,	points,	min_length,	max_length		verbose
int main(int argc, char* argv[])
{	
	cout << "# of arguments found: " << argc << endl;
	/*cout << "argv[1]: " << atoi(argv[1]) << endl
		<< "argv[2]: " << argv[2] << endl
		<< "argv[3]: " << argv[3] << endl
		<< "argv[4]: " << argv[4] << endl
		<< "argv[5]: " << argv[5] << endl
		<< "argv[6]: " << argv[6] << endl
		<< "argv[7]: " << argv[7] << endl
		<< "argv[8]: " << argv[8] << endl;
	cout << *argv << endl;
	*/
	string timestamp = get_timestamp();

	bool verbose = false;
	
	int num_turn_real = 0;
	int target_real_turn_num = 40;
	ofstream readme;
	string readme_time = get_date();

	if (OptimalAgent::ep == NULL) {
		std::cout << stderr << "\nCan't start MATLAB engine\n";
		return EXIT_FAILURE;
	}
	else
	{
		std::cout << "engine started successfully!" << endl << endl;
		// Load the data initially using the static function load() of OptimalAgent class
		// OptimalAgent::loadmat();
		OptimalAgent::loadmats();
	}

	srand(unsigned int(time(NULL)));
	// unsigned int time_int = int(time(NULL));
	// srand(time_int);

	// The variables
	std::cout.setf(ios::showpoint);
	std::cout.setf(ios::fixed);
	std::cout.precision(1);

	// Loading state-action table map (utable)
	string mapfile_in = "..\\data\\mapfile.txt";
	string mapfile_out = "..\\data\\mapfile_"+timestamp+".txt";
	OptimalAgent::loadmap(mapfile_in);

	std::cout << "\nMap size: " << OptimalAgent::utable.size() << endl << endl;

	// In this TCE, we have 3 participants (num_players = 3). 
	// The breakpoint is to save created data in every 10,000,000th data in seperated files with the date. 
	int num_players = 3, games, min_glength, max_glength, num_turns, whose_turn = 0, breakpoint = 10000000;
	unsigned long long int points;
	// int agents_type;
	int agent_types[3];
	bool optimal_exist = false;

	vector<Agent*> players;		// An agent pointer vector is to keep pointers to the players
	RandomAgent *rAgent[3];
	OptimalAgent *oAgent[3];
	RealAgent *reAgent[3];

	// The information part that will be entered through the keyboard.
	if (argc !=9) {
		std::cout << "Please enter the necessary information below " << endl
			<< "for Triadic Cooperative Experiment (TCE)" << endl
			<< "(The number of players is 3 by default.)" << endl << endl << "___ Agents' types: ___"
			<< endl << "Please type 1 to create a random agent. This agent will play randomly."
			<< endl << "Please type 2 to create an optimal agent. This agent will play optimally." << endl
			<< endl << "Your choice of agents' types (type 1(Random), 2(Optimal) or 3(Real))\t\t:\n";
		// int agent_types[3];

		for (int agt = 0; agt < 3; agt++)
		{
			if (agt == 0)
			{
				string string_input;
				cout << "Agent" << agt + 1 << " type [3]: ";
				getline(cin, string_input);
				if (string_input.empty()) agent_types[agt] = 3;
				else
					agent_types[agt] = stoi(string_input);
			}
			else if (agt == 1)
			{
				string string_input;
				cout << "Agent" << agt + 1 << " type [1]: ";
				getline(cin, string_input);
				if (string_input.empty()) agent_types[agt] = 1;
				else
					agent_types[agt] = stoi(string_input);
			}
			if (agt == 2)
			{
				string string_input;
				cout << "Agent" << agt + 1 << " type [1]: ";
				getline(cin, string_input);
				if (string_input.empty()) agent_types[agt] = 1;
				else
					agent_types[agt] = stoi(string_input);
			}

			// cin >> agent_types[agt];
		}
		



		// cin >> agents_type;


		// cin >> games;
		string string_input;
		std::cout << "The number of games\t\t\t\t\t[1000]: ";
		getline(cin, string_input);
		if (string_input.empty()) games = 1000;
		else
			games = stoi(string_input);

		// cin >> points;
		string_input = "";
		std::cout << "The points/money that players will have initially\t[100]: ";
		getline(cin, string_input);
		if (string_input.empty()) points = 100;
		else
			//points = stoi(string_input);
			points = stoull(string_input);


		// cin >> min_glength;
		string_input = "";
		std::cout << "Minimum game length\t\t\t\t\t[6]: ";
		getline(cin, string_input);
		if (string_input.empty()) min_glength = 6;
		else
			min_glength = stoi(string_input);

		// cin >> max_glength;
		string_input = "";
		std::cout << "Maximum game length\t\t\t\t\t[15]: ";
		getline(cin, string_input);
		if (string_input.empty()) max_glength = 15;
		else
			max_glength = stoi(string_input);


		string_input = "";
		std::cout << "Verbose [0]: ";
		getline(cin, string_input);
		if (string_input.empty()) verbose = false;
		else
			if (string_input == "1") verbose = true;

/*

		// string readme_filename = "..\\data\\readme_" + readme_time + "_" + to_string(games) + ".txt";
		string readme_filename = "..\\data\\readme_" + timestamp + "_" + to_string(games) + ".txt";
		readme.open(readme_filename);		// A file to write to initial value for variables 
		//(agents' types, the # of games, points, min game length, max game length = agents_type, games, points, min_glength, max_glength))

		// writing the initial values for the variables to the readme.txt
		readme << "Agents' types\n ";

		for (int agt = 0; agt < 3; agt++)
		{
			readme << "Agent" << agt+1 << "'s type: ";
			if (agent_types[agt] == 1)
			{
				readme << "Random" << endl;
				rAgent[agt] = new RandomAgent;
				switch (agt)
				{
				case 0:
					rAgent[agt]->set_name("P");
					break;
				case 1:
					rAgent[agt]->set_name("O1");
					break;
				case 2:
					rAgent[agt]->set_name("O2");
					break;
				default:
					break;
				}
				players.push_back(rAgent[agt]);
			}
			else if (agent_types[agt] == 3)
			{
				readme << "Real" << endl;
				reAgent[agt] = new RealAgent;
				switch (agt)
				{
				case 0:
					reAgent[agt]->set_name("P");
					break;
				case 1:
					reAgent[agt]->set_name("O1");
					break;
				case 2:
					reAgent[agt]->set_name("O2");
					break;
				default:
					break;
				}
				players.push_back(reAgent[agt]);
			}

			else
			{
				readme << "Optimal" << endl;
				oAgent[agt] = new OptimalAgent;
				switch (agt)
				{
				case 0:
					oAgent[agt]->set_name("P");
					break;
				case 1:
					oAgent[agt]->set_name("O1");
					break;
				case 2:
					oAgent[agt]->set_name("O2");
					break;
				default:
					break;
				}
				players.push_back(oAgent[agt]);
				optimal_exist = true;
			}
		}
		
		if (optimal_exist)
		{
			readme << "The number of games\t: " << games << endl
				<< "The points\t\t: " << points << endl
				<< "Minimum game length\t: " << min_glength << endl
				<< "Maximum game length\t: " << max_glength << endl;
		}

		*/
	}
	else
	{
		agent_types[0] = atoi(argv[1]);
		agent_types[1] = atoi(argv[2]);
		agent_types[2] = atoi(argv[3]);

		games = atoi(argv[4]);
		points = atoi(argv[5]);

		min_glength = atoi(argv[6]);
		max_glength = atoi(argv[7]);
		if (atoi(argv[8]) == 1) verbose = true;
	}



	string rl_games_filename = "..\\data\\rl_games_" + timestamp + "_";
	for (int ags = 0; ags < 3; ags++)
	{
		if (agent_types[ags] == 1) rl_games_filename += "R";
		else if (agent_types[ags] == 2) rl_games_filename += "O";
		else rl_games_filename += "Re";
	}
	rl_games_filename += ".txt";

	ofstream fout(rl_games_filename);		// A file to write to (state, action, reward, next state)



	// CREATING ALL AGENTS

	// string readme_filename = "..\\data\\readme_" + readme_time + "_" + to_string(games) + ".txt";
	string readme_filename = "..\\data\\readme_" + timestamp + "_" + to_string(games) + ".txt";
	readme.open(readme_filename);		// A file to write to initial value for variables 
								//(agents' types, the # of games, points, min game length, max game length = agents_type, games, points, min_glength, max_glength))

								// writing the initial values for the variables to the readme.txt
	readme << "Agents' types\n ";

	for (int agt = 0; agt < 3; agt++)
	{
		readme << "Agent" << agt + 1 << "'s type: ";
		if (agent_types[agt] == 1)
		{
			readme << "Random" << endl;
			rAgent[agt] = new RandomAgent;
			switch (agt)
			{
			case 0:
				rAgent[agt]->set_name("P");
				break;
			case 1:
				rAgent[agt]->set_name("O1");
				break;
			case 2:
				rAgent[agt]->set_name("O2");
				break;
			default:
				break;
			}
			players.push_back(rAgent[agt]);
		}
		else if (agent_types[agt] == 3)
		{
			readme << "Real" << endl;
			reAgent[agt] = new RealAgent;
			switch (agt)
			{
			case 0:
				reAgent[agt]->set_name("P");
				break;
			case 1:
				reAgent[agt]->set_name("O1");
				break;
			case 2:
				reAgent[agt]->set_name("O2");
				break;
			default:
				break;
			}
			players.push_back(reAgent[agt]);
		}

		else
		{
			readme << "Optimal" << endl;
			oAgent[agt] = new OptimalAgent;
			switch (agt)
			{
			case 0:
				oAgent[agt]->set_name("P");
				break;
			case 1:
				oAgent[agt]->set_name("O1");
				break;
			case 2:
				oAgent[agt]->set_name("O2");
				break;
			default:
				break;
			}
			players.push_back(oAgent[agt]);
			optimal_exist = true;
		}
	}

	if (optimal_exist)
	{
		readme << "The number of games\t: " << games << endl
			<< "The points\t\t: " << points << endl
			<< "Minimum game length\t: " << min_glength << endl
			<< "Maximum game length\t: " << max_glength << endl;
	}

	// END OF CREATING ALL AGENTS





	// START GAMES

	// The game is being played for num_turns turns (in range 0-6)
	unsigned int seed = 0;
	int i, i_min = 0, i_max = games - 1, ng = 0;
	time_t now = time(NULL), game_start, game_end, all_games_start, all_games_end, all_games_end2;


	all_games_start = time(NULL);
	int ng_all = 0;

	// for (int on = 0; on < 3; on++) cout << players[on]->get_name() << endl;
	// cout << endl;

	for (i = i_min; i <= i_max; i++)
	{
		if (verbose) cout << "\nNew game...\n\n";
		ng = 0;
		game_start = time(NULL);

		std::cout.flush();
		int turn_count;

		num_turns = rand() % (max_glength - min_glength + 1) + min_glength;
		turn_count = num_turns;

		//cout << "min_glength: " << min_glength << endl << "max_glength: " << max_glength << endl << "num_turns: " << num_turns << endl << endl;

		// It plays a single game
		while (turn_count-- > 0)
		{
			ng++;
			ng_all++;
			whose_turn = rand() % num_players;		// It assigns random player.
			// cout << "WHOSE TURN: " << whose_turn << endl;
			fout << players[whose_turn]->get_points() << " " << players[(whose_turn + 1) % 3]->get_points() << " " << players[(whose_turn + 2) % 3]->get_points()
				<< " " << num_turns - turn_count << " " << whose_turn << " ";

			act_pair act = players[whose_turn]->play(players, num_turns - turn_count, whose_turn, verbose);		// It calls actual virtual play function.

			fout << act.action << " " << act.p << " ";

			if (turn_count == 0) fout << players[whose_turn]->get_points() << endl;
			else fout << 0 << endl;

			// Real player test data recording
			if (whose_turn == 0) num_turn_real++;
			if (num_turn_real >= target_real_turn_num) break;
		}
		// End of a single game
		if (verbose)
		{
			cout << "\nEnd of game." << endl;
			players[0]->print(players, num_turns - turn_count - 1, 0);
			cout << endl;
		}

		// system("pause");

		// It resets points to 100 for the next game.
		for (int k = 0; k < num_players; k++)
		{
			players[k]->set_points(100);
		}

		if (i % breakpoint == 0 && i != 0)
		{
			string ltime = get_date();
			ltime += "_" + to_string(i) + ".txt";

			fout.close();
			// std::system(("copy ..\\data\\rl_games"+timestamp+".txt ..\\data\\" + ltime + " > nul 2>&1").c_str());
			std::system(("copy " + rl_games_filename + " "+rl_games_filename+"_"+to_string(i/breakpoint)+" > nul 2>&1").c_str());
			// fout.open("rl_games.txt");
			fout.open(rl_games_filename);
		}

		game_end = time(NULL);
		all_games_end2 = time(NULL);
		// if (i % 10 == 0) cout << 100 * static_cast<float>(i) / games << "% (" << i/1000 << "/" << games/1000 << ") (" << ng/(game_end-game_start) << " records/s)" << "\r" << std::flush;
		std::cout << 100 * static_cast<float>(i) / games << "% ("
			<< i << "/" << games << ") (" << ng / (game_end - game_start + 1) << " rps)"
			<< " (" << ng_all / (all_games_end2 - all_games_start + 1.0) << " gen. rps) ";

		if (i > 0 && i % 2 == 0)
		{
			std::cout << " (Map saved: " << OptimalAgent::utable.size() << ")";
			OptimalAgent::savemap(mapfile_out);
		}
		std::cout << "                                                \r";
		std::cout << std::flush;

		if (num_turn_real >= target_real_turn_num) break;
	}

	// END GAMES

	all_games_end = time(NULL);
	std::cout << 100 * static_cast<float>(i) / games << "% (" << i << "/" << games << ") (" << ng_all / (all_games_end - all_games_start + 1) << " rps)" << "                    \r" << std::flush;

	OptimalAgent::savemap(mapfile_out);
	std::cout << "\nMap saved: " << OptimalAgent::utable.size() << endl;
		
	string ltime = get_date();
	ltime += "_"+to_string(i)+".txt";

	fout.close();
	// std::system(("copy ..\\data\\rl_games.txt ..\\data\\" + ltime + " > nul 2>&1").c_str());
	std::system(("copy " + rl_games_filename + " " + rl_games_filename + "_" + to_string(i / breakpoint) + " > nul 2>&1").c_str());
	time_t end = time(NULL);

	readme << "Time\t\t\t: ";
	if ((end - now) < 60)
		readme << end - now << " sec(s)" << endl;
	else if ((end - now) >= 60)
		readme << (end - now) / 60 << " min(s)" << endl;
	else if ((end - now) >= 3600)
		readme << (end - now) / 3600 << " hour(s)" << endl;
	else if ((end - now) >= 86400)
		readme << (end - now) / 86400 << " day(s)" << endl;

	readme.close();
	system("pause");

	engEvalString(OptimalAgent::ep, "close;");
	engEvalString(OptimalAgent::ep, "exit;");
	engClose(OptimalAgent::ep);

	std::cout << endl << end - now << endl << endl;
	system("pause");
	return 0;
}

string get_date()
{
	struct tm newtime;
	char am_pm[] = "AM";
	__time64_t long_time;
	char timebuf[26];
	errno_t err;

	// Get time as 64-bit integer.
	_time64(&long_time);
	// Convert to local time.
	err = _localtime64_s(&newtime, &long_time);
	if (err)
	{
		printf("Invalid argument to _localtime64_s.");
		exit(1);
	}
	// Convert to an ASCII representation. 
	err = asctime_s(timebuf, 26, &newtime);
	if (err)
	{
		printf("Invalid argument to asctime_s.");
		exit(1);
	}
	strftime(timebuf, 26, "%Y_%m_%d", &newtime);
	return timebuf;
}

string get_timestamp()
{
	struct tm newtime;
	char am_pm[] = "AM";
	__time64_t long_time;
	char timebuf[26];
	errno_t err;

	// Get time as 64-bit integer.
	_time64(&long_time);
	// Convert to local time.
	err = _localtime64_s(&newtime, &long_time);
	if (err)
	{
		printf("Invalid argument to _localtime64_s.");
		exit(1);
	}
	// Convert to an ASCII representation. 
	err = asctime_s(timebuf, 26, &newtime);
	if (err)
	{
		printf("Invalid argument to asctime_s.");
		exit(1);
	}
	strftime(timebuf, 26, "%Y%m%d_%H%M%S", &newtime);
	return timebuf;
}

//string get_timestamp2()
//{
//	time_t	now = time(0);
//	struct tm tstruct;
//	char buf[80];
//	tstruct = *localtime(&now);
//	strftime(buf, sizeof(buf), "%Y%m%d_%H%M%S", &tstruct);
//	return buf;
//}