#pragma once

#include <iostream>
#include "agent.h"
using namespace std;

class RealAgent : public Agent
{
public:
	RealAgent();
	RealAgent(string newName, unsigned long long int newPoint, int newNum_turns);

	~RealAgent() { }

	virtual act_pair play(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose);
};