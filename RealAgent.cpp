#include <iostream>
#include <string>
#include "RealAgent.h"
using namespace std;

RealAgent::RealAgent() : Agent()
{ }

RealAgent::RealAgent(string newName, unsigned long long int newPoint, int newNum_turns) : Agent(newName, newPoint, newNum_turns)
{ }

act_pair RealAgent::play(vector<Agent*> &my_agents, int num_turns, int whose_turn, bool verbose)
{
	vector<int> targets;	// It keeps eligible target ids except the player.

							// It fills up the targets vector.
	for (unsigned int i = 0; i < my_agents.size(); i++)
		if (i != whose_turn)
		{
			targets.push_back(i);
		}

	my_agents[whose_turn]->print(my_agents, num_turns, whose_turn);

	cout << "Choose a target player to give your money: ";
	int target_num;
	cin >> target_num;

	// int targeti = rand() % targets.size();						// It assigns a random id in the targets.
	// int target_num = targets[targeti];							// The actual target id in the players
	// unsigned int action = rand() % Agent::actions.size();		// It takes a random action from the action set.
	unsigned int action;
	while (true)
	{
		cout << "Please enter action: ";
		cin >> action;
		if (action % 10 == 0 && action >= 0 && action <= 100) break;
		else cout << "Wrong action.\n";
	}

	Agent *me = my_agents[whose_turn];							// A pointer for the player
	Agent *target = my_agents[target_num];						// A pointer for the target

	unsigned long long int points = me->get_points() * action / 100;				// The actual points in action

	me->rem_points(points);										// The points are removed from the player.
	target->add_points(3 * points);								// 3 x points are added to the target

	return act_pair((target_num - whose_turn) % 3, action);
}